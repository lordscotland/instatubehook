#include "logger.h"
#include "subjc.h"

@interface EXTERNAL
-(NSArray*)folders;
-(void)getVideo:(NSString*)videoID;
-(id)initWithVideo:(id)video folder:(id)folder;
-(id)mainController;
@end

@interface ITSDelegate : NSObject @end
@implementation ITSDelegate
+(void)service:(id)service didFailWithError:(NSError*)error {
  UIAlertView* alert=[[UIAlertView alloc]
   initWithTitle:error.localizedDescription message:nil delegate:nil
   cancelButtonTitle:NSLocalizedStringFromTableInBundle(@"Cancel",nil,
    [NSBundle bundleWithIdentifier:@"com.apple.UIKit"],@"Cancel")
   otherButtonTitles:nil];
  [alert show];
  [alert release];
}
+(void)service:(id)service didReceiveVideoList:(NSArray*)videos {
  if(!videos.count){return;}
  id video=[videos objectAtIndex:0];
  NSArray* folders=[(id)[%c(ABDataManager) defaultManager] folders];
  id folder=folders.count?[folders objectAtIndex:0]:nil;
  UINavigationController* nav=[(id)[UIApplication sharedApplication].delegate mainController];
  UIViewController* sub=[[%c(VideoDetailViewController) alloc] initWithVideo:video folder:folder];
  [nav pushViewController:sub animated:YES];
  [sub release];
}      
@end

%hook TBAppDelegate
-(BOOL)application:(UIApplication*)application openURL:(NSURL*)URL sourceApplication:(NSString*)identifier annotation:(id)annotation {
  if([URL.scheme isEqualToString:@"youtube"]){
    id service=[[%c(YouTubeService) alloc] init];
    [service setDelegate:[ITSDelegate class]];
    [service getVideo:URL.resourceSpecifier];
    [service release];
    return YES;
  }
  return %orig;
}
%end

